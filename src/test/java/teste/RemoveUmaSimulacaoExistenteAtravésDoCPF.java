package teste;

import base.BaseAPI;
import io.restassured.RestAssured;
import org.junit.Test;

public class RemoveUmaSimulacaoExistenteAtravésDoCPF extends BaseAPI {

    @Test
    public void consultarCPFQuePossueOuNaoRestricao(){

        RestAssured.given()
                .when()
                .delete("http://localhost:8080/api/v1/simulacoes/66414919004")
                .then()
                .log()
                .all()
                .statusCode(200);
    }
}
