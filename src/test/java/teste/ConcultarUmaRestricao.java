package teste;

import base.BaseAPI;
import io.restassured.RestAssured;
import org.junit.Test;

public class ConcultarUmaRestricao extends BaseAPI {
    @Test
    public void consultarCPFQuePossueOuNaoRestricao(){

        RestAssured.given()
                .when()
                .get("http://localhost:8080/api/v1/restricoes/97093236014")
                .then()
                .log()
                .all()
                .statusCode(200);
    }
}

