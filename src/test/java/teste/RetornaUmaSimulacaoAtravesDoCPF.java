package teste;

import base.BaseAPI;
import io.restassured.RestAssured;
import org.junit.Test;

public class RetornaUmaSimulacaoAtravesDoCPF extends BaseAPI {

    @Test
    public void consultarCPFQuePossueOuNaoRestricao() {

        RestAssured.given()
                .when()
                .contentType("application/json")
                .body("{\"nome\": \"João\", \"cpf\": 66414919004, \"email\": \"joao@gmail.com\", \"valor\": 11000.00, \"parcelas\": 3, \"seguro\": true}")
                .get("http://localhost:8080/api/v1/simulacoes/66414919004")
                .then()
                .log()
                .all()
                .statusCode(200);
    }

}
