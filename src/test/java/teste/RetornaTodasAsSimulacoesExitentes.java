package teste;

import base.BaseAPI;
import io.restassured.RestAssured;
import org.junit.Test;

public class RetornaTodasAsSimulacoesExitentes extends BaseAPI {

    @Test
    public void consultarCPFQuePossueOuNaoRestricao() {

        RestAssured.given()
                .when()
                .get("http://localhost:8888/api/v1/simulacoes")
                .then()
                .log()
                .all()
                .statusCode(200);
    }
}
