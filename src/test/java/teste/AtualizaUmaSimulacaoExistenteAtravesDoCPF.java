package teste;

import base.BaseAPI;
import io.restassured.RestAssured;
import org.junit.Test;

public class AtualizaUmaSimulacaoExistenteAtravesDoCPF extends BaseAPI {
    @Test
    public void Atualiza(){
    RestAssured.given()
            .when()
                .contentType("application/json")
                .body("{ \"nome\": \"Fulano de Tal\", \"cpf\": 97093236014, \"email\": \"email@email.com\", \"valor\": 1200, \"parcelas\": 4, \"seguro\": true}")
                .put("http://localhost:8080/api/v1/simulacoes/97093236014")
                .then()
                .log()
                .all()
                .statusCode(200);
   }
}
